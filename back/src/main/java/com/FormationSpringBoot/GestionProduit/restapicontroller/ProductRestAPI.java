package com.FormationSpringBoot.GestionProduit.restapicontroller;


import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.FormationSpringBoot.GestionProduit.entities.Produit;
import com.FormationSpringBoot.GestionProduit.service.IServiceCategorie;
import com.FormationSpringBoot.GestionProduit.service.IServiceProduit;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/apiproduct")
@AllArgsConstructor
@CrossOrigin(origins="http://localhost:4200")
public class ProductRestAPI {
	IServiceProduit sp;
	IServiceCategorie sc;
	
	//http://localhost:8080/apiproduct/all
	@GetMapping("/all")
	public List<Produit> getAllProducts(){
		return sp.getAllProducts();
	}
	//http://localhost:8080/apiproduct/chercher/
	@GetMapping("/chercher/{mc}")
	public List<Produit> getProducts(@PathVariable("mc") String mc){
		return sp.getProductsByMC(mc);
	}
	@GetMapping("/get/{id}")
	public Produit getProductsid(@PathVariable("id") Integer id){
		return sp.getProduct(id);
	}
	@GetMapping(path = "/getImage/{id}",produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getImage(@PathVariable("id") int id)throws IOException {
		return sp.getImage(id);
	}
	//http://localhost:8080/apiproduct/add
	@PostMapping("/add")
	public void saveProduct(@RequestParam("pr") String p, @RequestParam("file") MultipartFile mf) throws IOException {
		Produit pp = new ObjectMapper().readValue(p, Produit.class);
		sp.saveProduit(pp, mf);
	}
	//http://localhost:8080/apiproduct/delete/9
	@DeleteMapping("/delete/{id}")
	public void supProducts(@PathVariable int id)throws IOException{
		 sp.supprimerProducts(id);
	}
	//http://localhost:8080/apiproduct/update
	@PostMapping("/update")
	public void modifierProduit(@RequestParam("pr") String p,@RequestParam("file") MultipartFile mf)throws IOException{
		saveProduct(p,mf);
	}
	
}
